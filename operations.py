import config
import os
import commands
from network import Network
def downloadTorrent(torrentID):
    fullURI='/download.php?id='+str(torrentID)
    torrentContent=Network.getURI(fullURI)
    torrentFilename=config.download['torrent_dir']+'/'+str(torrentID)+'.torrent'

    torrentFile=open(torrentFilename,"wb")
    torrentFile.write(torrentContent)
    torrentFile.close()

    cmd=config.download['downloader_cmd'];
    cmd=cmd.replace('%torrent',torrentFilename);
    cmd=cmd.replace('%download_dir',config.download['download_dir']);

    status, output = commands.getstatusoutput(cmd)
    if status == 0:
        return 0
    if 'duplicate' in output:
        return 1
    return 255
