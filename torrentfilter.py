from config import rules
def is_eq(lhs,rhs):
    return lhs==rhs

def is_lt(lhs,rhs):
    return lhs<rhs

def is_gt(lhs,rhs):
    return lhs>rhs

def is_ct(lhs,rhs):
    return rhs in lhs

class TorrentFilter:
    def filterTorrents(self,torrents):
        ret=[]
        for torrent in torrents:
            if self.checkTorrent(torrent):
                ret.append(torrent)
        return ret

    def checkTorrent(self,torrent):
        for rule in rules:
            verdict=True
            for ruleitem in rule:
                lhs=getattr(torrent,ruleitem[0])
                op=ruleitem[1]
                rhs=ruleitem[2]
                if op=='eq':
                    verdict=verdict and is_eq(lhs,rhs) 
                elif op=='lt':
                    verdict=verdict and is_lt(lhs,rhs) 
                elif op=='gt':
                    verdict=verdict and is_gt(lhs,rhs) 
                elif op=='ct':
                    verdict=verdict and is_ct(lhs,rhs) 
                else:
                    raise Exception("unrecognized operator")
                if verdict==False:
                    break
            if verdict==True:
                return True
        return False
                

