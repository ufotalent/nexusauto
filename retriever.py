from network import Network
from BeautifulSoup import BeautifulSoup
from torrent import Torrent
import time
class Retriever:
    torrentNodes=[]
    def retrieve(self):
        try:
            html=Network.getURI('/torrents.php').replace('\n','');
        except Exception:
            print '[',time.ctime(),']'            
            print 'Error while fetching page'
            return 
        torrentsSoup=BeautifulSoup(html);
        self.htmlNode=torrentsSoup.contents[1]
        self.tableBodyNode=self.htmlNode.find('table',attrs={'class':'torrents'})
        self.torrentNodes = []
        for i in range(1,len(self.tableBodyNode.contents)):
            self.torrentNodes.append(self.tableBodyNode.contents[i]);

    def getTorrents(self):
        ret=[]
        for node in self.torrentNodes:
            torrent = Torrent()
            torrent.loadFromHTMLNode(node);
            ret.append(torrent);
        return ret

