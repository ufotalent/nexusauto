config = {
    'siteurl': 'http://www.nexushd.org',
}

login = {
    'uri': '/takelogin.php',
    'form': {
        'username': 'ufotalent',
        'password': 'wanxinyitc',
    }
}

download = {
    'torrent_dir':'/home/ufotalent/tmp/nexusauto',
    'download_dir':'/home/ufotalent/Download/nexusauto',
    'downloader_cmd':'transmission-remote -a %torrent -w %download_dir',
}

rules=[
    [
        ['promotion','eq','twoupfree'],
    ],
    [
        ['promotion','eq','free'],
    ],
    [
        ['pin','eq',True],
        ['category','eq','Movies'],
    ],
    [
        ['pin','eq',True],
        ['category','eq','HQ Audio'],
    ],
    [
        ['title','ct','The Big Bang Theory S'],
        ['category','eq','TV Series'],
    ],
    [
        ['title','ct','Arrow S'],
        ['category','eq','TV Series'],
    ],
    [
        ['title','ct','Game of Thrones S'],
        ['category','eq','TV Series'],
    ],
    [
        ['title','ct','The Vampire Diaries S'],
        ['category','eq','TV Series'],
    ],
    [
        ['title','ct','HappyCamp'],
        ['category','eq','TV Shows'],
    ],
    [
        ['title','ct','Nikita S'],
        ['category','eq','TV Series'],
    ],
    [
        ['commentNum','gt',3],
        ['size','lt',10*1024*1024*1024],
    ],
    [
        ['uploadNum','lt',3],
        ['downloadNum','gt',10],
    ],
]

truncationWords=['1080p','1080i','720p','SD','Lossless']

downloadedTorrentStorageFileName='downloaded.pkl'
