class Torrent:
    promotion="none"
    pin=False
    category=""
    title=""
    description=""
    downloadLink=""
    torrentID=0
    commentNum=0
    duration=0
    size=0
    uploadNum=0
    downloadNum=0
    completeNum=0
    uploader=""
    
    def loadFromHTMLNode(self,node):
        if node.has_key('class'):
            self.promotion=node['class'];
        else:
            self.promotion="none"
        self.promotion=self.promotion.replace('_bg','')

        self.category=(node.contents[0].find('img'))['title']

        title_embednode=node.contents[1].find('td',{'class':'embedded'});
        self.pin=(title_embednode.find('img',{'class':'sticky'})!=None)
        self.title=title_embednode.find('a')['title']
        if len(title_embednode.contents)>2:
            self.description=unicode(title_embednode.contents[len(title_embednode.contents)-1].string)
        else:
            self.description=""

        self.downloadLink=node.contents[1].find('td',{'width':'20','class':'embedded'}).contents[0]['href'];
        self.torrentID=int(self.downloadLink.replace("download.php?id=",""));

#        print self.category, self.title.encode('utf-8'), self.description.encode('utf-8'), self.downloadLink,self.torrentID
        
        self.commentNum=int(unicode(node.contents[2].find('a').string.replace(',','')))

        duration_spannode=node.contents[3].find('span')
        self.duration=getsec(unicode(duration_spannode.contents[0]))
        if len(duration_spannode.contents)>2:
            self.duration=self.duration+getsec(unicode(duration_spannode.contents[2]))
#        print self.title.encode('utf-8'), self.commentNum, self.duration

        size_ratio={'KB':1024,'MB':1024**2,'GB':1024**3,'TB':1024**4,'PB':1024**5}
        self.size=int(float(node.contents[4].contents[0])*size_ratio[node.contents[4].contents[2]])
#        print self.size

        upload_subnode=node.contents[5].contents[0]
        upload_anode=upload_subnode.find('a');
        if upload_anode!=None:
            fontnode=upload_anode.find('font')
            if fontnode==None:
                self.uploadNum=int(upload_anode.string.replace(',',''))
            else:
                self.uploadNum=int(fontnode.string.replace(',',''))
        else:
            self.uploadNum=0

        download_node=node.contents[6]
        download_anode=download_node.find('a');
        if download_anode!=None:
            self.downloadNum=int(download_anode.string.replace(',',''))
        else:
            self.downloadNum=0

        complete_node=node.contents[7]
        complete_bnode=complete_node.find('b');
        if complete_bnode!=None:
            self.completeNum=int(complete_bnode.string.replace(',',''))
        else:
            self.completeNum=0
#        print self.uploadNum, self.downloadNum, self.completeNum

        uploader_node=node.contents[8]
        uploader_bnode=uploader_node.find('b');
        if uploader_bnode!=None:
            self.uploader=unicode(uploader_bnode.string)
        else:
            self.uploader="anonymous"
#        print self.uploader

def getsec(strtime):
    time_ratio={
            u"\u5e74":60*60*24*365,
            u"\u6708":60*60*24*30,
            u"\u5929":60*60*24,
            u"\u65f6":60*60,
            u"\u5206":60,
            u"\u79d2":1,
            }
    l=len(strtime)
    num = strtime[:l-1]
    suffix = strtime[l-1]
    try:
        ret=int(num)*time_ratio[suffix]
    except Exception:
        ret=0
    return ret
