import os.path
import pickle
from config import truncationWords
def truncateTitle(title):
    maxindex=-1
    for kw in truncationWords:
        index=title.rfind(kw)
        if index>maxindex:
            maxindex=index
    if maxindex!=-1:
        title=title[:maxindex]
    return title

class DuplicationDetector:
    cache=[]
    storageFileName=""
    def __init__(self, storageFileName):
        self.storageFileName=storageFileName
        self.loadCache()

    def registerTorrent(self, torrent):
        if self.haveTorrent(torrent,False):
            raise Exception('torrent already registered')
        self.cache.append(torrent)
        self.saveCache()

    def haveTorrent(self, torrent, checkSimilar = True):
        for candidate in self.cache:
            if torrent.torrentID == candidate.torrentID :
                return True
            if checkSimilar and truncateTitle(torrent.title) == truncateTitle(candidate.title):
                return True
        return False

    def loadCache(self):
        if os.path.exists(self.storageFileName)==False:
            return 
        inputf=open(self.storageFileName,'rb')
        self.cache=pickle.load(inputf)
        inputf.close()

    def saveCache(self):
        outputf=open(self.storageFileName,'wb')
        pickle.dump(self.cache,outputf)
        outputf.close()

