import network
import retriever
import operations
import torrentfilter
import duplicationdetector
import config
import time
import sys
def simpleErase(new,old):
    ret=[]
    for newitem in new:
        ex=False
        for olditem in old:
            if newitem.torrentID==olditem.torrentID and newitem.promotion==olditem.promotion and newitem.description==olditem.description and newitem.title==olditem.title:
                ex=True
                break
        if not ex:
            ret.append(newitem);
    return ret

r=retriever.Retriever();
tf=torrentfilter.TorrentFilter()
dt=duplicationdetector.DuplicationDetector(config.downloadedTorrentStorageFileName)

oldtorrents=[]
while True:
    r.retrieve();
    newtorrents=r.getTorrents()
#    difftorrents=simpleErase(newtorrents,oldtorrents)
    difftorrents=newtorrents
    filteredtorrents=tf.filterTorrents(difftorrents)
    oldtorrents=newtorrents
    for candidatetorrent in filteredtorrents:
        if not dt.haveTorrent(candidatetorrent, not ('free' in candidatetorrent.promotion)):
            print '[',time.ctime(),']'
            op=operations.downloadTorrent(candidatetorrent.torrentID);
            if op == 0:
                dt.registerTorrent(candidatetorrent)
                print '|---successfully download torrent with title:'
                print candidatetorrent.title.encode('utf-8')
            elif op == 1:
                dt.registerTorrent(candidatetorrent)
                print 'register already downloaded torrent:'
                print candidatetorrent.title.encode('utf-8')
            else:
                print 'download',candidatetorrent.title.encode('utf8'),'failed'
        else:
            pass
#            print candidatetorrent.title.encode('utf8'),'passed filter but exist downloaded similar torrent'
    sys.stdout.flush()
    time.sleep(20)

