import urllib2
import urllib
import cookielib
import config

class Network:
    jar = cookielib.FileCookieJar("cookies")
    opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(jar))
    loggedIn = False
#    @staticmethod
#    def init():
#        if Network.loggedIn:
#            return
#        logIn(Network.opener);
#        Network.loggedIn = True;


    @staticmethod
    def getURI(url):
        if Network.loggedIn==False:
            logIn(Network.opener)
            Network.loggedIn = True;
        response = Network.opener.open(config.config['siteurl']+url)
        return response.read();

def isLoggedIn(opener):
    url = config.config['siteurl']
    response = opener.open(url)
    print response.geturl()
    if (response.geturl() == config.config['siteurl']+'/login.php') :
        return False
    else:
        return True

def logIn(opener):
    data = urllib.urlencode(config.login['form'])
    response = opener.open(config.config['siteurl']+config.login['uri'],data);
#    print response.read();

